#!/bin/bash
#
# Compile and run a C source file.
# Compiled program is placed in ./bin/
#
[ -d 'bin/' ] || mkdir 'bin/'

source="${1-src/main.c}"			# src/main.c if no argument provided
output="$(basename "$source")"
output="${output::-2}" 				# remove .c extension

shift

$CC -std=c17 -o bin/"$output" "$source" && ./bin/"$output" "$@"
