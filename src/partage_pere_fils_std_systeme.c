#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#define _POSIX_C_SOURCE 200112L

int main()
{
    int outfd = open("fileout",O_RDWR+O_CREAT); // pour résoudre le problème lors de la réécriture : placer les droits ex: 0600
    int infd = open("filein", O_RDWR);
    FILE*  outfile = fdopen(outfd,"w");

    char buffer[3];

    fprintf(outfile,"123");
    write(outfd,"456",3);
    read(infd, buffer, 3);

    printf("Before fork read %s\n", buffer);
    int p = fork();

    if (p > 0) {
        write(outfd,"p7p8",4);
        fprintf(outfile,"p9\n");
        read(infd, buffer, 3);
        printf("Parent after fork read %s\n", buffer);

    } else {
        write(outfd,"c7c8",4);
        fprintf(outfile,"c9\n");
        read(infd, buffer, 3);
        printf("Child after fork read %s\n", buffer);
    }
}
