#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define bufsize 256

int main(const int argc, char** argv)
{
    if (argc > 1)
    {
        int file = open(argv[1], O_WRONLY | O_APPEND);

        if (file > 0) {
            /* dup2 is atomic, better than close then dup which should be
               mutex protected in a multithreaded environment. */
            dup2(file, STDOUT_FILENO);
        } else {
            printf("Please pass an existing and writable file as argument.\n");
            perror("open error");
            exit(EXIT_FAILURE);
        }
    }

    char buffer[bufsize];
    int read_n;

    while ((read_n = read(STDIN_FILENO, buffer, bufsize)))
    {
        if (!(write(STDOUT_FILENO, buffer, read_n))) {
            perror("write error");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
